const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql');
const connection = mysql.createConnection({
    host : 'localhost',
    user : 'Dormitory',
    password : 'Dormitory',
    database : 'DormitorySystem'
})

connection.connect();
const express = require('express');
const req = require('express/lib/request');
const app = express()
const port = 4000


/* Middleware for Authenticating User Token */
function authenticateToken (req, res, next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if(err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}


/*----------------------- INSERT INTO --------------------------*/
app.post("/add_tenant", (req, res) => {
    let tenant_name = req.query.tenant_name
    let tenant_surname = req.query.tenant_surname

    let query = `INSERT INTO Tenant
                    (TenantName, TenantSurname)
                    VALUES ('${tenant_name}','${tenant_surname}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding tenant succesful"
              })
        }
    })
})


/*----------------------- SELECT ---------------------------*/
app.get("/list_tenant",(req, res) => {
    let query = "SELECT * from Tenant"
    connection.query( query, (err, rows) => {
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error querying from tenant db"
                    })
        }else{
            res.json(rows)
        }
    })
})


/*------------------------ UPDATE ----------------------------*/
app.post("/update_tenant", (req, res) => {
    
    let room_number = req.query.room_number
    let tenant_name = req.query.tenant_name
    let tenant_surname = req.query.tenant_surname

    let query = `UPDATE Tenant SET
                    TenantName='${tenant_name}',
                    TenantSurname='${tenant_surname}'
                    WHERE RoomNumber=${room_number}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Updating tenant succesful"
              })
        }
    })
})


/*------------------------- DELETE ----------------------------*/
app.post("/delete_tenant", (req, res) => {
    
    let room_number = req.query.room_number

    let query = `DELETE FROM Tenant 
                    WHERE RoomNumber=${room_number}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error deleting record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Deleting record succesful"
              })
        }
    })
})


/*------------------------- REGISTER ----------------------------*/
app.post("/register_tenant", (req, res) => {
    
    let tenant_name = req.query.tenant_name
    let tenant_surname = req.query.tenant_surname
    let tenant_username = req.query.tenant_username
    let tenant_password = req.query.tenant_password
    
    bcrypt.hash(tenant_password, SALT_ROUNDS, (err,hash) => {

        let query = `INSERT INTO Tenant
                    (TenantName, TenantSurname, Username, Password)
                    VALUES ('${tenant_name}','${tenant_surname}','${tenant_username}','${hash}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding new user succesful"
              })
        }
    })
  })

})

/*--------------------------- LOGIN --------------------------*/

app.post("/login" ,(req,res) => {

    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM tenant WHERE Username='${username}'`
    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json( {
                    "status" : "400",
                    "message" : "Error inserting data into db"
                    })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(user_password,db_password, (err,result) => {
                if (result){
                    console.log(err)
                   let payload = {

                       "username" : rows[0].Username,
                       "user_id" : rows[0].RoomNumber,
                   }
                   console.log(payload)
                   let token = jwt.sign(payload,TOKEN_SECRET,{expiresIn : '1d'})
                   res.send(token)
                }else{res.send("Invalid username / password")}
            })
        }

    })
})

/*--------------------- list_reg_by_roomnumber -------------------------*/
app.get("/list_reg_by_roomnumber", authenticateToken, (req, res) => {
  
    let room_number = req.user.user_id
    
    
    let query = `
    
    SELECT tenant.RoomNumber, dormitoryaddress.DormitoryName, registration.RegistrationId
            
    FROM runner, runningevent, registration

    WHERE (tenant.RoomNumber = registration.RoomNumber) AND
        (registration.DormitoryID = dormitoryaddress.DormitoryID) AND
        (tenant.RoomNumber = ${room_number})
            `
        connection.query( query, (err, rows) => {
            if(err){
                res.json({
                        "status" : "400",
                        "message" : "Error querying from running db"
                        })
            }else{
                res.json(rows)
            }
        })

})


app.listen(port, () => {
    console.log(`Now starting DormitorySyatem Backend ${port}`)
})

